
#include "controller.h"

static struct iirFilter PI = {{0},{0}}; // init a PI IIR filter with all elements equal to 0

float Controller(float error) {

    // Ki = 100; Kp = 0.1 -> Tustin
    PI.xs[1] = PI.xs[0];
    PI.xs[0] = error;
    PI.ys[1] = PI.ys[0];
    PI.ys[0] = PI.ys[1] + 0.04505*PI.xs[0] - 0.04495*PI.xs[1];

    // Anti wind-up
    if (PI.ys[0] < 0) { PI.ys[0] = 0; }
    if (PI.ys[0] > 1) { PI.ys[0] = 1; }

    return PI.ys[0];
}

void ResetController() {
    PI.xs[0] = 0.0;
    PI.xs[1] = 0.0;
    PI.ys[0] = 0.0;
    PI.ys[1] = 0.0;
}

struct phaseDelays ControlLaw(float m, float d) {
    float D1 = 0.0;
    float D2 = 0.0;
    float FI = 0.0;

    float d2 = d*d;
    if (d > 1) // Boost mode
    {
        float m1 = (d+1)/(d2+d+1);
        if (m <= m1)
        {
            D1 = d*m;
            D2 = m;
            FI = d/(d+1)*D1;
        }
        else
        {
            D1 = 1+(m-1)/d2;
            D2 = m;
            FI = (1-d+(d+1)*D1-D2)/2;
        }
    }
    else // Buck mode
    {
        float m2 = (d2+d)/(d2+d+1);
        if (m <= m2)
        {
            D1 = m;
            D2 = m/d;
            FI = d/(d+1)*D1;
        }
        else
        {
            D1 = m;
            D2 = 1-d2+m*d2;
            FI = (1-d+(d+1)*D1-D2)/2;
        }
    }

    // D1, D2, FI ∈ [0,1]
    // TODO Add checks on values
    struct phaseDelays p;

    p.D1 = D1;
    p.D2 = FI;
    p.D3 = FI+D2;

    return p;
}
