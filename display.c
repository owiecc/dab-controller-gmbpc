/*
 * display.c
 *
 *  Created on: Jun 24, 2020
 *      Author: szymon
 */

#include "DSP28x_Project.h"     // Device header file and examples include file

#define N_SEGMENTS   4

static const long int symbols[12] = {
    0b11000000, // 0
    0b11111001, // 1
    0b10100100, // 2
    0b10110000, // 3
    0b10011001, // 4
    0b10010010, // 5
    0b10000010, // 6
    0b11111000, // 7
    0b10000000, // 8
    0b10010000, // 9
    0b10001110, // F
    0b10001100, // P
};

struct display
{
    int Vi;
    int Vo;
};

static struct display disp;

void display(int Vi, int Vo)
{
    disp.Vi = Vi;
    disp.Vo = Vo;
}

// update_display() is run with every interrupt of Timer0. At each invocation
// a different 7 segment display is activated.
void update_display(void)
{
    static int segment_id = 0;
    long int segment_num = 0;

    // set display segments
    long int segment_comm = 0;
    switch (segment_id) {
           case 0:
               {
                   segment_comm = 0b1110;
                   segment_num = disp.Vi / 10;
                   break;
               }
           case 1:
               {
                   segment_comm = 0b1101;
                   segment_num = disp.Vi % 10;
                   break;
               }
           case 2:
               {
                   segment_comm = 0b1011;
                   segment_num = disp.Vo / 10;
                   break;
               }
           case 3:
               {
                   segment_comm = 0b0111;
                   segment_num = disp.Vo % 10;
                   break;
               }
           default: {
               segment_comm = 0x00000000;
               segment_num = 0b11111111;
           }
        }
    // reset display segments
    GpioDataRegs.GPACLEAR.all = 0x0FFF0000; // clear bits 16-23 and 24-27
    // set the current symbol on the current display
    GpioDataRegs.GPASET.all = segment_comm << 24 | symbols[segment_num] << 16;
    // next segment
    segment_id = (segment_id + 1) % N_SEGMENTS;
}
