/*
 * input.c
 *
 *  Created on: Jun 25, 2020
 *      Author: szymon
 */

#include "DSP28x_Project.h"     // Device header file and examples include file
#include "input.h"

#define BUTTON_DEBOUNCE_CHECKS 20

enum button_states {Up=0,Down=1,Pressed=2,Released=3};

int button_pressed()
{
    static int btn_count = 0;
    static int btn_state = Up;
    int port = (GpioDataRegs.GPADAT.all >> 8) & 0b01111111; // only (14-8) inputs are connected to buttons

    // count how long the button is held [0..BUTTON_DEBOUNCE_CHECKS]
    if (port > 0 & btn_count < BUTTON_DEBOUNCE_CHECKS) { btn_count++; }
    if (port == 0 & btn_count > 0) { btn_count--; }

    // Get pressed button ID
    // shifts the data until 1 if found on the LSB
    // number of shifts = number of button pressed
    int btn_pressed = -1;
    while (port > 0) {
        port = port >> 1;
        btn_pressed++;
    }

    // button state machine
    switch (btn_state)
    {
        case Up:
            if (btn_count == BUTTON_DEBOUNCE_CHECKS) {btn_state = Pressed;} break;
        case Pressed:
            btn_state = Down; break;
        case Down:
            if (btn_count == 0) {btn_state = Released;} break;
        case Released:
            btn_state = Up; break;
    }

    return (btn_state == Pressed) ? btn_pressed : -1;
}
