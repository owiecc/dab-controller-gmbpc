// Included Files
#include "DSP28x_Project.h"     // Device header file and examples include file
#include "pwm.h"
#include "display.h"
#include "input.h"
#include "controller.h"

// Function Prototypes
__interrupt void cpu_timer0_isr(void);
__interrupt void adc_isr(void);

// Defines
#define EPWM_DB   0x000F

#define FNOM   110
#define DTNOM   100

enum converter_states {Idle,Run,FaultDebug};
static enum converter_states converter_state;

enum buttons {On=0, LL=2, L=1, Zero=3, R=4, RR=5, Off=6};

void main(void)
{
    // Initialize System Control: PLL, WatchDog, enable Peripheral Clocks
    InitSysCtrl();

    // Define ADCCLK clock frequency ( less than or equal to 25 MHz )
    // Assuming InitSysCtrl() has set SYSCLKOUT to 150 MHz
    EALLOW;
    SysCtrlRegs.HISPCP.all = 0x3; // HSPCLK = SYSCLKOUT/2*ADC_MODCLK2 = 150/(2*3) = 25.0 MHz
    EDIS;

    InitGpio();

    // Clear all interrupts and initialize PIE vector table: Disable CPU interrupts
    DINT;
    InitPieCtrl();
    IER = 0x0000;
    IFR = 0x0000;
    InitPieVectTable();
    EALLOW;
    PieVectTable.TINT0 = &cpu_timer0_isr;
    PieVectTable.ADCINT = &adc_isr;
    EDIS;

    // Initialize all the device peripherals:
    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;
    EDIS;

    memcpy(&RamfuncsRunStart, &RamfuncsLoadStart, (Uint32)&RamfuncsLoadSize);
    InitFlash();

    InitEPwmCommon(FNOM,DTNOM);
    InitAdc();
    InitCpuTimers();
    ConfigCpuTimer(&CpuTimer0, 150, 5000); // 5ms
    CpuTimer0Regs.TCR.all = 0x4000; // TSS bit = 0

    EALLOW;
    SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;
    EDIS;

    // User specific code, enable interrupts
    
    // Enable CPU INT1 which is connected to CPU-Timer 0
    IER |= M_INT1;

    // Enable ADC in the PIE: Group 1 interrupt 6
    PieCtrlRegs.PIEIER1.bit.INTx6 = 1;
    // Enable TINT0 in the PIE: Group 1 interrupt 7
    PieCtrlRegs.PIEIER1.bit.INTx7 = 1;

    // Enable global Interrupts and higher priority real-time debug events
    EINT;       // Enable Global interrupt INTM
    ERTM;       // Enable Global real-time interrupt DBGM

    // Configure ADC
    AdcRegs.ADCMAXCONV.all = 0x0001;       // Setup 2 conversions on SEQ1
    AdcRegs.ADCCHSELSEQ1.bit.CONV00 = 0x4; // Setup ADCINA4 as 1st SEQ1
    AdcRegs.ADCCHSELSEQ1.bit.CONV01 = 0x6; // Setup ADCINA6 as 2nd SEQ1

    // Enable SOCA from ePWM to start SEQ1
    AdcRegs.ADCTRL2.bit.EPWM_SOCA_SEQ1 = 1;
    AdcRegs.ADCTRL2.bit.INT_ENA_SEQ1 = 1;  // Enable SEQ1 interrupt (every EOS)

    // Init variables
    converter_state = Idle;

    for(;;)
    {
        int btn = button_pressed();

        switch (converter_state)
        {
            case Idle:
                switch (btn)
                {
                    case On: EnablePWM(), converter_state = Run;
                }
                break;
            case Run:
                switch (btn)
                {
                    case Off: DisablePWM(), converter_state = Idle;
                }
                break;
            case FaultDebug:
                switch (btn)
                {
                    case Zero: DisablePWM(), converter_state = Idle;

                }
            default: __asm("          NOP");
        }
    }
}

__interrupt void cpu_timer0_isr(void)
{
    update_display();
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;
}

__interrupt void adc_isr(void)
{
    Uint16 Voadc = AdcRegs.ADCRESULT0 >> 4;
    Uint16 Viadc = AdcRegs.ADCRESULT1 >> 4;

    const float Voref = 52.0; // DC-grid nominal voltage

    // Scale ADC
    #define R1 20000
    #define R2 1000
    const float ADCgain = (R1+R2)/R2*3.0/4095; // voltage divider on the input R2/R1;

    float Vi = ADCgain * Viadc;
    float Vo = ADCgain * Voadc;

    display(Vi,Vo);

    // Controller

    if (converter_state == Run)
        {

        if (Vi<30 || Vi>60 || Vo<30 || Vo>60)
        {
            DisablePWM();
            converter_state = FaultDebug;
        }

        float m = Controller(Voref - Vo); // PI controller
        float d = Vo/Vi; // Voltage gain

        // GMBPC control law

        struct phaseDelays p = ControlLaw(m, d);

        // Modulator

        SetPhases(p.D1, p.D2, p.D3);
    }
    else
    {
        npulses = 0;
        //Vo2 = 48;
        ResetController();
        SetPhases(0.0, 0.0, 0.0);
    }

    // Reinitialize for next ADC sequence
    AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;         // Reset SEQ1
    AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;       // Clear INT SEQ1 bit
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;   // Acknowledge interrupt to PIE
}
