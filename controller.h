
#ifndef CONTROLLER_H_
#define CONTROLLER_H_

struct phaseDelays {
    float D1, D2, D3;
};

struct iirFilter {
    float xs[2];
    float ys[2];
};

float Controller(float);
void ResetController(void);
struct phaseDelays ControlLaw(float, float);

#endif /* CONTROLLER_H_ */
