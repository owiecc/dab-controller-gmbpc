/*
 * pwm.h
 *
 *  Created on: Jun 24, 2020
 *      Author: szymon
 */

#ifndef PWM_H_
#define PWM_H_

#endif /* PWM_H_ */

void InitEPwmCommon(int,int);
void SetPhases(float,float,float);
void DisablePWM(void);
void EnablePWM(void);
